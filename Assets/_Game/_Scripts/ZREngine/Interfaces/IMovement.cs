using UnityEngine;

namespace ZREngine.Interfaces
{
    public interface IMovement
    {
        public float MovementSpeed { get; set; }
        public float RotationSpeed { get; set; }
        public float RotationOffset { get; set; }
        public Rigidbody2D RigidBody2D { get;}

        public void Move();
        public void LookAtDirection();
    }
}