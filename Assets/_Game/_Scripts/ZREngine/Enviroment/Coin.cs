using System;
using Unity.Netcode;
using UnityEngine;
using Unity.Networking;

namespace ZREngine.Enviroment
{
    public class Coin : NetworkBehaviour
    {
        public void OnCoinCollected()
        {
           DespawnObjectServerRpc();
        }


        [ServerRpc(RequireOwnership = false)]
        public void DespawnObjectServerRpc()
        {
            NetworkObject.Despawn();
        }
         
        
    }
}