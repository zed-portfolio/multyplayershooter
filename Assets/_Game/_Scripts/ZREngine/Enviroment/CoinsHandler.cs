using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ZREngine.Enviroment
{
    public class CoinsHandler : NetworkBehaviour
    {
        [SerializeField]
        private Coin _coinPrefab;

        [SerializeField]
        private float _spawnCointPerSecond;
        
        
        private float _timeCounter;


        private void Update()
        {
            if (IsServer)
            {
                _timeCounter += Time.deltaTime;
                if (_timeCounter >= _spawnCointPerSecond)
                {
                    _timeCounter = 0;
                    SpawnCoinServerRpc();
                }
            }
        }


        [ServerRpc(RequireOwnership = false)]
        private void SpawnCoinServerRpc()
        {
            var gm = Instantiate(_coinPrefab, GetRandomSpawnPosition(), Quaternion.identity);
            gm.NetworkObject.SpawnWithOwnership(NetworkManager.Singleton.LocalClientId);
        }
        

        private Vector3 GetRandomSpawnPosition()
        {
            var rndX = Random.Range(0, Screen.width);
            var rndY = Random.Range(0, Screen.height);
            var wordPos = Camera.main.ScreenToWorldPoint(new Vector3(rndX, rndY, Camera.main.nearClipPlane));
            return wordPos;
        }
    }
}