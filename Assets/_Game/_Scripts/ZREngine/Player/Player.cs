using Unity.Netcode;
using UnityEngine;
using ZREngine.Classes;
using ZREngine.Enviroment;

namespace ZREngine.Player
{
    public class Player : NetworkBehaviour
    {
        private GameUI _gameUI;
        public GameUI GameUI
        {
            get
            {
                if (!_gameUI)
                    _gameUI = FindObjectOfType<GameUI>();

                return _gameUI;
            }
        }
        
        private int _coinsCount;

        public void AddCoin()
        {
            if (!IsLocalPlayer) return;
            _coinsCount++;
            GameUI.SetCoinsCount(_coinsCount);
        }
        
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Coin"))
            {
                AddCoin();
                other.GetComponent<Coin>().OnCoinCollected();
                print("Coin Collected");
            }
        }
        
        
        public override void OnNetworkSpawn() {
            if (!IsOwner) Destroy(this);
        }
    }
}