using UnityEngine;
using ZREngine.Classes;

namespace ZREngine.Player
{
    public class PlayerMovement : Movement
    {
        [SerializeField] 
        private float _screenBorder;
        
        private void FixedUpdate()
        {
            Move();
            PreventPlayer();
        }


        private void PreventPlayer()
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);

            if ((screenPosition.x < _screenBorder && RigidBody2D.velocity.x < 0) ||
                (screenPosition.x > Camera.main.pixelWidth - _screenBorder && RigidBody2D.velocity.x > 0))
            {
                RigidBody2D.velocity = new Vector2(0, RigidBody2D.velocity.y);
            }

            if ((screenPosition.y < _screenBorder && RigidBody2D.velocity.y < 0) ||
                (screenPosition.y > Camera.main.pixelHeight - _screenBorder && RigidBody2D.velocity.y > 0))
            {
                RigidBody2D.velocity = new Vector2(RigidBody2D.velocity.x, 0);
            }
        }
    }
}