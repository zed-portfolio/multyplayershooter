
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ZREngine.Classes
{
    public class GameUI : NetworkBehaviour
    {
        [SerializeField]
        private TMP_Text _coinsText;
        
        
        public void ExitFromServer()
        {
        }


        public void SetCoinsCount(int coins)
        {
            _coinsText.text = $"COINS: {coins}";
        }
    }
}