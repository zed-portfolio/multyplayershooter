using UnityEngine;
using ZREngine.Interfaces;
using Unity.Netcode;

namespace ZREngine.Classes
{
    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class Movement : NetworkBehaviour, IMovement
    {
        [field: Range(1, 10)]
        [field: SerializeField]
        public float MovementSpeed { get; set; }
        
        [field: Range(1, 10)]
        [field: SerializeField]
        public float RotationSpeed { get; set; }
        
        [field: SerializeField]
        public float RotationOffset { get; set; }

        private Rigidbody2D _rigidbody2D;
        public Rigidbody2D RigidBody2D
        {
            get
            {
                if (!_rigidbody2D)
                {
                    _rigidbody2D = GetComponent<Rigidbody2D>();
                }

                return _rigidbody2D;
            }
        }
        
        private Vector2 _direction =>
            new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        

        public void Move()
        {
            RigidBody2D.velocity = _direction * MovementSpeed;
            LookAtDirection();
        }
        
        public void LookAtDirection()
        {
            var angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg + RotationOffset;
            Quaternion newRotation = Quaternion.AngleAxis((angle), Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, RotationSpeed);
        }
    }
}